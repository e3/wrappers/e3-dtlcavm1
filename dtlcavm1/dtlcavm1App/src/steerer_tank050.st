/* DTL Steerer State Machine - Tank 5
# State Machine devoted to control the status and the health of DTL steerer sub-system :
#
# Principal States:
# - Initialization:
# 	* Initialize State Machine when EPICS application starts
# - Self-Check:
#       * Verify HW status (connections, devices\'' health, etc.)
# - Start-Up:
#       * Prepare the sub-system to perform operations
# - Idle:
#       * Machine is in attendance of external ok
# - Ready-for-operation:
#       * Machine prepared for operation
# - Failure:
#       * Machine goes in safe mode because of HW/SW problems
# - Maintenance:
#       * Status where the machine is under maintenance. Useful to indicate possible operation and works on the apparatus
# - Tank-Disabled:
#       * Status where tank is disabled by operator
#
#
# developed by Maurizio Montis | INFN-LNL
# mail: maurizio.montis@lnl.infn.it
# Added printTSMsg function to print Timestamp in stdout (Alfio - Mon 23 May 2022 09:31:26 AM CEST)
*/

program steerer_tank050

/*
# Libraries
*/

%%#include <stdio.h>
%%#include <string.h>
%%#include <unistd.h>
%%#include <stdarg.h>

/*
# Variables and Constants
*/

/* Tanks enabling system (from Orchestration application) */
int T_Enabling_System;
assign T_Enabling_System to "{SECTION_NAME}::TankEnb";
monitor T_Enabling_System;

/* ok status */
int T5_V1_OkStat;
assign T5_V1_OkStat  to "{SECTION_NAME}:PwrC-PSCV-001:OkStat";
monitor T5_V1_OkStat;
int T5_H1_OkStat;
assign T5_H1_OkStat  to "{SECTION_NAME}:PwrC-PSCH-001:OkStat";
monitor T5_H1_OkStat;
int T5_V2_OkStat;
assign T5_V2_OkStat  to "{SECTION_NAME}:PwrC-PSCV-002:OkStat";
monitor T5_V2_OkStat;
int T5_H2_OkStat;
assign T5_H2_OkStat  to "{SECTION_NAME}:PwrC-PSCH-002:OkStat";
monitor T5_H2_OkStat;

/* termal switches ok status */
int T5_V1_Termal_OkStat;
assign T5_V1_Termal_OkStat  to "{SECTION_NAME}:PwrC-PSCV-001:TS_OkStat";
monitor T5_V1_Termal_OkStat;
int T5_H1_Termal_OkStat;
assign T5_H1_Termal_OkStat  to "{SECTION_NAME}:PwrC-PSCH-001:TS_OkStat";
monitor T5_H1_Termal_OkStat;
int T5_V2_Termal_OkStat;
assign T5_V2_Termal_OkStat  to "{SECTION_NAME}:PwrC-PSCV-002:TS_OkStat";
monitor T5_V2_Termal_OkStat;
int T5_H2_Termal_OkStat;
assign T5_H2_Termal_OkStat  to "{SECTION_NAME}:PwrC-PSCH-002:TS_OkStat";
monitor T5_H2_Termal_OkStat;

/* termal switches enable commands */
int T5_V1_Termal_Enable_Cmd;
assign T5_V1_Termal_Enable_Cmd  to "{SECTION_NAME}:PwrC-PSCV-001:OnEnbCmd";
monitor T5_V1_Termal_Enable_Cmd;
int T5_H1_Termal_Enable_Cmd;
assign T5_H1_Termal_Enable_Cmd  to "{SECTION_NAME}:PwrC-PSCH-001:OnEnbCmd";
monitor T5_H1_Termal_Enable_Cmd;
int T5_V2_Termal_Enable_Cmd;
assign T5_V2_Termal_Enable_Cmd  to "{SECTION_NAME}:PwrC-PSCV-002:OnEnbCmd";
monitor T5_V2_Termal_Enable_Cmd;
int T5_H2_Termal_Enable_Cmd;
assign T5_H2_Termal_Enable_Cmd  to "{SECTION_NAME}:PwrC-PSCH-002:OnEnbCmd";
monitor T5_H2_Termal_Enable_Cmd;

/* 24V Ok Status */
int Rack010_24V;
assign Rack010_24V to "DTL-010Row:CnPw-U-003:V24PS_OKStat";
monitor Rack010_24V;
int Rack030_24V;
assign Rack030_24V to "DTL-010Row:CnPw-U-003:V24PS_OKStat";
monitor Rack030_24V;

/* Tank Main Ok Status */ 
int Tank_Total_Ok_Status;
assign Tank_Total_Ok_Status to "DTL-050::PwrPsOnEnb";
monitor Tank_Total_Ok_Status;

/* Thermo sensors temperature alarm */  /* minor alarm = 2 || major alarm = 3 */
int T5_V1_ThermoSensor_Severity;
assign T5_V1_ThermoSensor_Severity to "{SECTION_NAME}:EMR-TT-003:Temp.SEVR";
monitor T5_V1_ThermoSensor_Severity;
int T5_H1_ThermoSensor_Severity;
assign T5_H1_ThermoSensor_Severity to "{SECTION_NAME}:EMR-TT-005:Temp.SEVR";
monitor T5_H1_ThermoSensor_Severity;
int T5_V2_ThermoSensor_Severity;
assign T5_V2_ThermoSensor_Severity to "{SECTION_NAME}:EMR-TT-015:Temp.SEVR";
monitor T5_V2_ThermoSensor_Severity;
int T5_H2_ThermoSensor_Severity;
assign T5_H2_ThermoSensor_Severity to "{SECTION_NAME}:EMR-TT-017:Temp.SEVR";
monitor T5_H2_ThermoSensor_Severity;

/* Thermo sensors module error */
int T5_V1_ThermoSensor_Module_Error;
assign T5_V1_ThermoSensor_Module_Error to "{SECTION_NAME}:EMR-TT-003:HwErrAlrm";
monitor T5_V1_ThermoSensor_Module_Error;
int T5_H1_ThermoSensor_Module_Error;
assign T5_H1_ThermoSensor_Module_Error to "{SECTION_NAME}:EMR-TT-005:HwErrAlrm";
monitor T5_H1_ThermoSensor_Module_Error;
int T5_V2_ThermoSensor_Module_Error;
assign T5_V2_ThermoSensor_Module_Error to "{SECTION_NAME}:EMR-TT-015:HwErrAlrm";
monitor T5_V2_ThermoSensor_Module_Error;
int T5_H2_ThermoSensor_Module_Error;
assign T5_H2_ThermoSensor_Module_Error to "{SECTION_NAME}:EMR-TT-017:HwErrAlrm";
monitor T5_H2_ThermoSensor_Module_Error;


/* Impedance */
double T5_V1_Impedance;
assign T5_V1_Impedance  to "{SECTION_NAME}:BMD-CV-001:Imp";
monitor T5_V1_Impedance;
double T5_H1_Impedance;
assign T5_H1_Impedance  to "{SECTION_NAME}:BMD-CH-001:Imp";
monitor T5_H1_Impedance;
double T5_V2_Impedance;
assign T5_V2_Impedance  to "{SECTION_NAME}:BMD-CV-002:Imp";
monitor T5_V2_Impedance;
double T5_H2_Impedance;
assign T5_H2_Impedance  to "{SECTION_NAME}:BMD-CH-002:Imp";
monitor T5_H2_Impedance;

/* Delta Impedances */
double MAX_DELTA_DEVIANCE;      // max impedance deviation accepted before fault [Ohm]
assign MAX_DELTA_DEVIANCE to "DTL::MaxDeltaImp";
monitor MAX_DELTA_DEVIANCE;

double T5_V1_DeltaImpedance;
assign T5_V1_DeltaImpedance  to "{SECTION_NAME}:BMD-CV-001:DeltaImp";
monitor T5_V1_DeltaImpedance;
double T5_H1_DeltaImpedance;
assign T5_H1_DeltaImpedance  to "{SECTION_NAME}:BMD-CH-001:DeltaImp";
monitor T5_H1_DeltaImpedance;
double T5_V2_DeltaImpedance;
assign T5_V2_DeltaImpedance  to "{SECTION_NAME}:BMD-CV-002:DeltaImp";
monitor T5_V2_DeltaImpedance;
double T5_H2_DeltaImpedance;
assign T5_H2_DeltaImpedance  to "{SECTION_NAME}:BMD-CH-002:DeltaImp";
monitor T5_H2_DeltaImpedance;

/* Average Impedances */
double MAX_AVG_DEVIANCE;                // max impedance average accepted before fault [Ohm]
assign MAX_AVG_DEVIANCE to "DTL::MaxAvgImp";
monitor MAX_AVG_DEVIANCE;

double T5_V1_AvgImpedance;
assign T5_V1_AvgImpedance  to "{SECTION_NAME}:BMD-CV-001:AvgImp";
monitor T5_V1_AvgImpedance;
double T5_H1_AvgImpedance;
assign T5_H1_AvgImpedance  to "{SECTION_NAME}:BMD-CH-001:AvgImp";
monitor T5_H1_AvgImpedance;
double T5_V2_AvgImpedance;
assign T5_V2_AvgImpedance  to "{SECTION_NAME}:BMD-CV-002:AvgImp";
monitor T5_V2_AvgImpedance;
double T5_H2_AvgImpedance;
assign T5_H2_AvgImpedance  to "{SECTION_NAME}:BMD-CH-002:AvgImp";
monitor T5_H2_AvgImpedance;


/* PS Channel Enable Command */
int T5_V1_ChEnableCmd;
assign T5_V1_ChEnableCmd  to "{SECTION_NAME}:BMD-CV-001:EnbCmd";
monitor T5_V1_ChEnableCmd;
int T5_H1_ChEnableCmd;
assign T5_H1_ChEnableCmd  to "{SECTION_NAME}:BMD-CH-001:EnbCmd";
monitor T5_H1_ChEnableCmd;
int T5_V2_ChEnableCmd;
assign T5_V2_ChEnableCmd  to "{SECTION_NAME}:BMD-CV-002:EnbCmd";
monitor T5_V2_ChEnableCmd;
int T5_H2_ChEnableCmd;
assign T5_H2_ChEnableCmd  to "{SECTION_NAME}:BMD-CH-002:EnbCmd";
monitor T5_H2_ChEnableCmd;



/* PS Channel Enable Readback */
int T5_V1_ChEnable;
assign T5_V1_ChEnable  to "{SECTION_NAME}:BMD-CV-001:Enb";
monitor T5_V1_ChEnable;
int T5_H1_ChEnable;
assign T5_H1_ChEnable  to "{SECTION_NAME}:BMD-CH-001:Enb";
monitor T5_H1_ChEnable;
int T5_V2_ChEnable;
assign T5_V2_ChEnable  to "{SECTION_NAME}:BMD-CV-002:Enb";
monitor T5_V2_ChEnable;
int T5_H2_ChEnable;
assign T5_H2_ChEnable  to "{SECTION_NAME}:BMD-CH-002:Enb";
monitor T5_H2_ChEnable;



/* PS Channel Current Set */
double T5_V1_CurrentSet;
assign T5_V1_CurrentSet  to "{SECTION_NAME}:BMD-CV-001:Curr-S";
monitor T5_V1_CurrentSet;
double T5_H1_CurrentSet;
assign T5_H1_CurrentSet  to "{SECTION_NAME}:BMD-CH-001:Curr-S";
monitor T5_H1_CurrentSet;
double T5_V2_CurrentSet;
assign T5_V2_CurrentSet  to "{SECTION_NAME}:BMD-CV-002:Curr-S";
monitor T5_V2_CurrentSet;
double T5_H2_CurrentSet;
assign T5_H2_CurrentSet  to "{SECTION_NAME}:BMD-CH-002:Curr-S";
monitor T5_H2_CurrentSet;



/* PS Channel Current Set - Readback*/
double T5_V1_CurrentSet_RB;
assign T5_V1_CurrentSet_RB  to "{SECTION_NAME}:BMD-CV-001:Curr-RB";
monitor T5_V1_CurrentSet_RB;
double T5_H1_CurrentSet_RB;
assign T5_H1_CurrentSet_RB  to "{SECTION_NAME}:BMD-CH-001:Curr-RB";
monitor T5_H1_CurrentSet_RB;
double T5_V2_CurrentSet_RB;
assign T5_V2_CurrentSet_RB  to "{SECTION_NAME}:BMD-CV-002:Curr-RB";
monitor T5_V2_CurrentSet_RB;
double T5_H2_CurrentSet_RB;
assign T5_H2_CurrentSet_RB  to "{SECTION_NAME}:BMD-CH-002:Curr-RB";
monitor T5_H2_CurrentSet_RB;



/* PS Channel Current Measured*/
double T5_V1_Current;
assign T5_V1_Current  to "{SECTION_NAME}:BMD-CV-001:Curr-R";
monitor T5_V1_Current;
double T5_H1_Current;
assign T5_H1_Current  to "{SECTION_NAME}:BMD-CH-001:Curr-R";
monitor T5_H1_Current;
double T5_V2_Current;
assign T5_V2_Current  to "{SECTION_NAME}:BMD-CV-002:Curr-R";
monitor T5_V2_Current;
double T5_H2_Current;
assign T5_H2_Current  to "{SECTION_NAME}:BMD-CH-002:Curr-R";
monitor T5_H2_Current;



/* PS Channel Voltage Measured*/
double T5_V1_Voltage;
assign T5_V1_Voltage  to "{SECTION_NAME}:BMD-CV-001:Voltage";
monitor T5_V1_Voltage;
double T5_H1_Voltage;
assign T5_H1_Voltage  to "{SECTION_NAME}:BMD-CH-001:Voltage";
monitor T5_H1_Voltage;
double T5_V2_Voltage;
assign T5_V2_Voltage  to "{SECTION_NAME}:BMD-CV-002:Voltage";
monitor T5_V2_Voltage;
double T5_H2_Voltage;
assign T5_H2_Voltage  to "{SECTION_NAME}:BMD-CH-002:Voltage";
monitor T5_H2_Voltage;



/* PS Channel Reset Command */
int T5_V1_Reset_Cmd;
assign T5_V1_Reset_Cmd  to "{SECTION_NAME}:BMD-CV-001:Clr";
monitor T5_V1_Reset_Cmd;
int T5_H1_Reset_Cmd;
assign T5_H1_Reset_Cmd  to "{SECTION_NAME}:BMD-CH-001:Clr";
monitor T5_H1_Reset_Cmd;
int T5_V2_Reset_Cmd;
assign T5_V2_Reset_Cmd  to "{SECTION_NAME}:BMD-CV-002:Clr";
monitor T5_V2_Reset_Cmd;
int T5_H2_Reset_Cmd;
assign T5_H2_Reset_Cmd  to "{SECTION_NAME}:BMD-CH-002:Clr";
monitor T5_H2_Reset_Cmd;



/* Chassis Enable */
int Chassis5_enable;
assign Chassis5_enable to "DTL-030Row:PwrC-PSC-002:OnEnbCmd";
monitor Chassis5_enable;


/* Chassis Enable - readback */
int Chassis5_enable_RB;
assign Chassis5_enable_RB to "DTL-030Row:PwrC-PSC-002:OnEnbCmd-RB";
monitor Chassis5_enable_RB;



/* PS Line Enable */
int T5_V1_LineEnable;
assign T5_V1_LineEnable  to "{SECTION_NAME}:PwrC-PSCV-001:OnEnbCmd";
monitor T5_V1_LineEnable;
int T5_H1_LineEnable;
assign T5_H1_LineEnable  to "{SECTION_NAME}:PwrC-PSCH-001:OnEnbCmd";
monitor T5_H1_LineEnable;
int T5_V2_LineEnable;
assign T5_V2_LineEnable  to "{SECTION_NAME}:PwrC-PSCV-002:OnEnbCmd";
monitor T5_V2_LineEnable;
int T5_H2_LineEnable;
assign T5_H2_LineEnable  to "{SECTION_NAME}:PwrC-PSCH-002:OnEnbCmd";
monitor T5_H2_LineEnable;



/* PS Line Enable - readback */
int T5_V1_LineEnable_RB;
assign T5_V1_LineEnable_RB  to "{SECTION_NAME}:PwrC-PSCV-001:OnEnbCmd-RB";
monitor T5_V1_LineEnable_RB;
int T5_H1_LineEnable_RB;
assign T5_H1_LineEnable_RB  to "{SECTION_NAME}:PwrC-PSCH-001:OnEnbCmd-RB";
monitor T5_H1_LineEnable_RB;
int T5_V2_LineEnable_RB;
assign T5_V2_LineEnable_RB  to "{SECTION_NAME}:PwrC-PSCV-002:OnEnbCmd-RB";
monitor T5_V2_LineEnable_RB;
int T5_H2_LineEnable_RB;
assign T5_H2_LineEnable_RB  to "{SECTION_NAME}:PwrC-PSCH-002:OnEnbCmd-RB";
monitor T5_H2_LineEnable_RB;



/* Manual/Auto Control  */
int AutoControlCmd;
assign AutoControlCmd to "{SECTION_NAME}::SteererAutoCmd";
monitor AutoControlCmd;

int AutoControl_ActualStatus;
assign AutoControl_ActualStatus to "{SECTION_NAME}::SteererAutoCmd-RB";
monitor AutoControl_ActualStatus;


/* Start and Stop Operations */
int StartOperationCmd;
assign StartOperationCmd to "{SECTION_NAME}::SteererOpRun";
monitor StartOperationCmd;
int StopOperationCmd;
assign StopOperationCmd to "{SECTION_NAME}::SteererOpStop";
monitor StopOperationCmd;

/* Maintenance and fault control*/
int MaintenanceReset;
assign MaintenanceReset to "DTL::SteererMaintenanceRst";
monitor MaintenanceReset;
int FaultReset;
assign FaultReset to "DTL::SteererFaultRst";
monitor FaultReset;


/*
# Stages Code:
# 	0  -> INIT
# 	10 -> SELF-CHECK
# 	20 -> IDLE
# 	30 -> START-UP
# 	40 -> READY FOR OPERATION
# 	50 -> FAILURE
# 	60 -> MAINTENANCE
#   70 -> TANK DISABLED
*/
int StateMachineCode;
assign StateMachineCode to "{SECTION_NAME}::SteererStateMachine";

/*
# DTL Operational State Machine Code:
# 	0   -> INIT
# 	101 -> STAND-BY
# 	202 -> RF CONDITIONING
# 	303 -> RF START-UP
# 	404 -> READY FOR OPERATION
# 	505 -> NO ACCELERATED BEAM
# 	606 -> MAINTENANCE
*/
int dtlOperationalCode;
assign dtlOperationalCode to "{SECTION_NAME}::StateMachine";
monitor dtlOperationalCode;

/* Debug Mode: */
double DEBUG=1;

/* Print Timestamp in Stdout */
%{

  static void printTSMsg(const char * fmt, ...) {
    char buffer[4096];
    va_list args;
    va_start(args, fmt);
    vsprintf(buffer, fmt, args);
    va_end(args);
    epicsTimeStamp now_ts;
    epicsTimeGetCurrent( &now_ts);
    char nowText[40];
    nowText[0] = 0;
    epicsTimeToStrftime(nowText,sizeof(nowText),"%Y/%m/%d %H:%M:%S.%03f",&now_ts);
    printf("%s:: %s", nowText,buffer);
  }

}%

/*
# State Machine Definition
*/

ss  steerer_tank050 {

/* state INITIALIZATION	*/
	state initialization {
      entry {
        printTSMsg("\n\nPreliminary controls executed due to Application Startup - Intialization\n\n");
        StateMachineCode=0;
        pvPut(StateMachineCode);
      }

      when ( pvConnectCount()==pvAssignCount() ) {
        printTSMsg("[SEQ STEERER - TANK 5] All PVs connected\n");
        printTSMsg("[SEQ STEERER - TANK 5] Starting Self Check Process\n");

        /* Actual man|auto control initialization */
        AutoControl_ActualStatus = AutoControlCmd;
        pvPut(AutoControl_ActualStatus);

				/* Disable Start and Stop Operaton Commands */
				StartOperationCmd=0;
				pvPut(StartOperationCmd);
				StopOperationCmd=0;
				pvPut(StopOperationCmd);

        /* [Man|Auto] Control Actual Value initialization */
        AutoControl_ActualStatus = AutoControlCmd;
        if ( AutoControl_ActualStatus == 0 ) {
          printTSMsg("[SEQ STEERER - TANK 5] Manual|Auto Steerers Control readback initialized to: MANUAL\n");
        }
        else if ( AutoControl_ActualStatus == 1 ) {
          printTSMsg("[SEQ STEERER - TANK 5] Manual|Auto Steerers Control readback initialized to: AUTO\n");
        }
        pvPut(AutoControl_ActualStatus);
      } state self_check
	}


/* state SELF_CHECK	*/
	state self_check {
    entry {
        printTSMsg("\n[SEQ STEERER - TANK 5] Enter in SELF CHECK status\n");
        StateMachineCode=10;
        pvPut(StateMachineCode);

				if (T_Enabling_System == 1) {

						printTSMsg("[SEQ STEERER - TANK 5] 1. Channels Disable channels \n");
		        T5_V1_ChEnableCmd = 0;
		        pvPut(T5_V1_ChEnableCmd, SYNC);
						sleep(1);
		        T5_H1_ChEnableCmd = 0;
		        pvPut(T5_H1_ChEnableCmd, SYNC);
						sleep(1);
		        T5_V2_ChEnableCmd = 0;
		        pvPut(T5_V2_ChEnableCmd, SYNC);
						sleep(1);
		        T5_H2_ChEnableCmd = 0;
		        pvPut(T5_H2_ChEnableCmd, SYNC);
						sleep(3);

		        printTSMsg("[SEQ STEERER - TANK 5] 2. Channels Error Reset \n");
		        T5_V1_Reset_Cmd=0;
		        pvPut(T5_V1_Reset_Cmd, SYNC);
		        sleep(1);
		        T5_H1_Reset_Cmd=0;
		        pvPut(T5_H1_Reset_Cmd, SYNC);
		        sleep(1);
		        T5_V2_Reset_Cmd=0;
		        pvPut(T5_V2_Reset_Cmd, SYNC);
		        sleep(1);
		        T5_H2_Reset_Cmd=0;
		        pvPut(T5_H2_Reset_Cmd, SYNC);
		        sleep(1);

		        printTSMsg("[SEQ STEERER - TANK 5] 3. Enable Chassis \n");
		        Chassis5_enable=1;
		        pvPut(Chassis5_enable);
		        sleep(1);

		        printTSMsg("[SEQ STEERER - TANK 5] 4. Enable Lines \n");
		        T5_V1_Termal_Enable_Cmd=1;
		        pvPut(T5_V1_Termal_Enable_Cmd, SYNC);
		        sleep(3);
		        T5_H1_Termal_Enable_Cmd=1;
		        pvPut(T5_H1_Termal_Enable_Cmd, SYNC);
		        sleep(3);
		        T5_V2_Termal_Enable_Cmd=1;
		        pvPut(T5_V2_Termal_Enable_Cmd, SYNC);
		        sleep(3);
		        T5_H2_Termal_Enable_Cmd=1;
		        pvPut(T5_H2_Termal_Enable_Cmd, SYNC);
		        sleep(3);
				}
				else {
					printTSMsg("[SEQ STEERER - TANK 5] Self check initialization bypassed by Tank Disable Status \n");
				}
    }

		/* Tank Disabled  */
    when( T_Enabling_System == 0) {
        printTSMsg("[SEQ STEERER  - TANK 5] Tank 5 Disabled by Operator\n");
        printTSMsg("[SEQ STEERER  - TANK 5] System functional control disabled. Go to TANK DISABLED\n");
    } state tank_disabled

    /* Chassis disconnected (HW) */
    when ( (Chassis5_enable_RB == 0) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Error in chassis enable system: one or more chassis are not available\n");
        printTSMsg("[SEQ STEERER - TANK 5] System not passed self check: go to FAILURE \n");
    } state failure

    /* Thermal Switch problem */
    when ( (T5_V1_Termal_OkStat == 0) || (T5_H1_Termal_OkStat == 0) || (T5_V2_Termal_OkStat == 0) || (T5_H2_Termal_OkStat == 0) ) {
    /* Line disconnected (HW) */
        printTSMsg("[SEQ STEERER - TANK 5] Error in line enable system: thermal switch problem in one or more lines \n");
        printTSMsg("[SEQ STEERER - TANK 5] System not passed self check: go to FAILURE \n");
    } state failure

    /* 24V Error */
    when ( (Rack010_24V == 0) || (Rack030_24V == 0) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Error in 24V power racks. \n");
        printTSMsg("[SEQ STEERER - TANK 5] System not passed self check: go to FAILURE \n");
    } state failure

    /* no error in any line */
    when ( (T5_V1_Termal_OkStat == 1) && (T5_H1_Termal_OkStat == 1) && (T5_V2_Termal_OkStat == 1) && (T5_H2_Termal_OkStat == 1) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Self Check Passed. \n");
        printTSMsg("[SEQ STEERER - TANK 5] Enable steerers channels. \n");

        /* NOTE: when steerer is switched ON, the device goes to the last setpoint set. */
        /*       pay attention if the behavior is correct according to the general operations */

				if (T5_V1_ThermoSensor_Severity == 2 || T5_V1_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V1. Power supply not enabled.\n");
				}
				else {
					T5_V1_ChEnableCmd = 1;
					pvPut(T5_V1_ChEnableCmd, SYNC);
					sleep(3);
				}
				if (T5_H1_ThermoSensor_Severity == 2 || T5_H1_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H1. Power supply not enabled.\n");
				}
				else {
					T5_H1_ChEnableCmd = 1;
					pvPut(T5_H1_ChEnableCmd, SYNC);
					sleep(3);
				}
				if (T5_V2_ThermoSensor_Severity == 2 || T5_V2_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V2. Power supply not enabled.\n");
				}
				else {
					T5_V2_ChEnableCmd = 1;
					pvPut(T5_V2_ChEnableCmd, SYNC);
					sleep(3);
				}
				if (T5_H2_ThermoSensor_Severity == 2 || T5_H2_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H2. Power supply not enabled.\n");
				}
				else {
					T5_H2_ChEnableCmd = 1;
					pvPut(T5_H2_ChEnableCmd, SYNC);
					sleep(3);
				}
    } state start_up

  }


/* state STARTUP	*/
state start_up {
  entry {
    printTSMsg("\n[SEQ STEERER - TANK 5] Enter in START UP status\n");
    StateMachineCode=30;
    pvPut(StateMachineCode);
  }

	/* Tank Disabled  */
	when( T_Enabling_System == 0) {
			printTSMsg("[SEQ STEERER  - TANK 5] Tank 5 Disabled by Operator\n");
			printTSMsg("[SEQ STEERER  - TANK 5] System functional control disabled. Go to TANK DISABLED\n");
	} state tank_disabled

  /* Channels are not enabled */
  when ( (T5_V1_ChEnable == 3) || (T5_H1_ChEnable == 3) || (T5_V2_ChEnable == 3) || (T5_H2_ChEnable == 3) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] One or more channels are not enabled \n");
      //printTSMsg("[SEQ STEERER - TANK 5] Try to rescan the system status \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure //self_check

  when ( (Chassis5_enable_RB == 0) ) {
      printTSMsg("[SEQ STEERER - TANK 5] Error in chassis enable system: one or more chassis are not available\n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

  /* Thermal Switch problem */
  when ( (T5_V1_Termal_OkStat == 0) || (T5_H1_Termal_OkStat == 0) || (T5_V2_Termal_OkStat == 0) || (T5_H2_Termal_OkStat == 0) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] Error in line enable system: thermal switch problem in one or more lines \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

  /* 24V Error */
  when ( (Rack010_24V == 0) || (Rack030_24V == 0) ) {
      printTSMsg("[SEQ STEERER - TANK 5] Error in 24V power racks. \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

	/* Total enable error */
	when ( (T5_V1_OkStat == 0) || (T5_H1_OkStat == 0) || (T5_V2_OkStat == 0) || (T5_H2_OkStat == 0) ) {
			printTSMsg("[SEQ STEERER - TANK 5] Error in final channel ok status: one or more line are not providing Ok Status \n");
			printTSMsg("[SEQ STEERER - TANK 5] System not passed self check: go to FAILURE \n");
	} state failure

  /* Problems with steerers impedances */
  when ( (T5_V1_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_H1_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_V2_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_H2_AvgImpedance > MAX_AVG_DEVIANCE) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more stereres register HIGH IMPEDANCE DEVIANCE \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure


  /* No problems with steerers channels */
  when ( (T5_V1_ChEnable == 1) && (T5_H1_ChEnable == 1) && (T5_V2_ChEnable == 1) && (T5_H2_ChEnable == 1) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] Channels correctly enabled. Start setpoint reset. \n");
      T5_V1_CurrentSet = 0;
      pvPut(T5_V1_CurrentSet, SYNC);
      T5_H1_CurrentSet = 0;
      pvPut(T5_H1_CurrentSet, SYNC);
      T5_V2_CurrentSet = 0;
      pvPut(T5_V2_CurrentSet, SYNC);
      T5_H2_CurrentSet = 0;
      pvPut(T5_H2_CurrentSet, SYNC);


  } state idle

}


/* state IDLE   	*/
state idle {
  entry {
    printTSMsg("\n[SEQ STEERER - TANK 5] Enter in IDLE status\n");
    StateMachineCode=20;
    pvPut(StateMachineCode);
    sleep(2);

    StopOperationCmd=0;
    pvPut(StopOperationCmd);
  }

	/* Tank Disabled  */
	when( T_Enabling_System == 0) {
			printTSMsg("[SEQ STEERER  - TANK 5] Tank 5 Disabled by Operator\n");
			printTSMsg("[SEQ STEERER  - TANK 5] System functional control disabled. Go to TANK DISABLED\n");
	} state tank_disabled

  /* Channels are not enabled */
  when ( (T5_V1_ChEnable == 3) || (T5_H1_ChEnable == 3) || (T5_V2_ChEnable == 3) || (T5_H2_ChEnable == 3) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] One or more channels are not enabled \n");
      printTSMsg("[SEQ STEERER - TANK 5] Try to rescan the system status \n");
  } state failure

  when ( (Chassis5_enable_RB == 0) ) {
      printTSMsg("[SEQ STEERER - TANK 5] Error in chassis enable system: one or more chassis are not available\n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

  /* Thermal Switch problem */
  when ( (T5_V1_Termal_OkStat == 0) || (T5_H1_Termal_OkStat == 0) || (T5_V2_Termal_OkStat == 0) || (T5_H2_Termal_OkStat == 0) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] Error in line enable system: thermal switch problem in one or more lines \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

  /* 24V Error */
  when ( (Rack010_24V == 0) || (Rack030_24V == 0) ) {
      printTSMsg("[SEQ STEERER - TANK 5] Error in 24V power racks. \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

  /* Problems with steerers impedances */
  when ( (T5_V1_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_H1_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_V2_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_H2_AvgImpedance > MAX_AVG_DEVIANCE) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more stereres register HIGH IMPEDANCE DEVIANCE \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

  /* Some PVs are disconnected */
  when (pvConnectCount() != pvAssignCount()) {
    printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more PVs are not available for the state machine. \n");
    printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
  } state failure

	/* Error with high temperature measured related to steerers */
	when (T5_V1_ThermoSensor_Severity == 2 || T5_H1_ThermoSensor_Severity == 2 || T5_V2_ThermoSensor_Severity == 2 || T5_H2_ThermoSensor_Severity == 2) {
			printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more thermo-sensors indicate HIGH TEMPERATURE \n");
			printTSMsg("[SEQ STEERER - TANK 5] System not ok: switch off steerers with error and go to FAILURE \n");

			if (T5_V1_ThermoSensor_Severity == 2 || T5_V1_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V1. Power supply disabled.\n");
				T5_V1_ChEnableCmd = 0;
				pvPut(T5_V1_ChEnableCmd, SYNC);
				sleep(1);
			}
			if (T5_H1_ThermoSensor_Severity == 2 || T5_H1_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H1. Power supply disabled.\n");
				T5_H1_ChEnableCmd = 0;
				pvPut(T5_H1_ChEnableCmd, SYNC);
				sleep(1);
			}
			if (T5_V2_ThermoSensor_Severity == 2 || T5_V2_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V2. Power supply disabled.\n");
				T5_V2_ChEnableCmd = 0;
				pvPut(T5_V2_ChEnableCmd, SYNC);
				sleep(1);
			}
			if (T5_H2_ThermoSensor_Severity == 2 || T5_H2_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H2. Power supply disabled.\n");
				T5_H2_ChEnableCmd = 0;
				pvPut(T5_H2_ChEnableCmd, SYNC);
				sleep(1);
			}
	} state failure


	/* Error with Broken sensor temperature related to steerers */
	when (T5_V1_ThermoSensor_Module_Error == 1 || T5_H1_ThermoSensor_Module_Error == 1 || T5_V2_ThermoSensor_Module_Error == 1 || T5_H2_ThermoSensor_Module_Error == 1) {
			printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more thermo-sensors indicate HIGH TEMPERATURE \n");
			printTSMsg("[SEQ STEERER - TANK 5] System not ok: switch off steerers with error and go to FAILURE \n");

			if (T5_V1_ThermoSensor_Severity == 2 || T5_V1_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V1. Power supply disabled.\n");
				T5_V1_ChEnableCmd = 0;
				pvPut(T5_V1_ChEnableCmd, SYNC);
				sleep(1);
			}
			if (T5_H1_ThermoSensor_Severity == 2 || T5_H1_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H1. Power supply disabled.\n");
				T5_H1_ChEnableCmd = 0;
				pvPut(T5_H1_ChEnableCmd, SYNC);
				sleep(1);
			}
			if (T5_V2_ThermoSensor_Severity == 2 || T5_V2_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V2. Power supply disabled.\n");
				T5_V2_ChEnableCmd = 0;
				pvPut(T5_V2_ChEnableCmd, SYNC);
				sleep(1);
			}
			if (T5_H2_ThermoSensor_Severity == 2 || T5_H2_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H2. Power supply disabled.\n");
				T5_H2_ChEnableCmd = 0;
				pvPut(T5_H2_ChEnableCmd, SYNC);
				sleep(1);
			}
	} state failure

	/* Possibility to modify [MAN|AUTO] control during operations without changing state*/
	when ( AutoControl_ActualStatus != AutoControlCmd ) {
			AutoControl_ActualStatus = AutoControlCmd;
			pvPut(AutoControl_ActualStatus);
			if ( AutoControl_ActualStatus == 0 ) {
				printTSMsg("[SEQ STEERER - TANK 5] Control Configuration changed to MANUAL .\n");
			}
			else if ( AutoControl_ActualStatus == 1 ) {
				printTSMsg("[SEQ STEERER - TANK 5] Control Configuration changed to AUTOMATIC.\n");
			}
	} state idle

  /* Execute chosen configuration [man|auto] */
  when ( (StartOperationCmd == 1) ) {
      if ( (AutoControl_ActualStatus == 0) && (AutoControl_ActualStatus == AutoControlCmd) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Start Control Operations in MANUAL Mode\n");
      }
      else if ( (AutoControl_ActualStatus == 1) && (AutoControl_ActualStatus == AutoControlCmd) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Start Control Operations in AUTOMATIC Mode\n");
      }
      else if ( (AutoControl_ActualStatus == 1) && (AutoControl_ActualStatus != AutoControlCmd) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Updated control configuration\n");
        printTSMsg("[SEQ STEERER - TANK 5] Start Control Operations in MANUAL Mode\n");
        AutoControl_ActualStatus = AutoControlCmd;
        pvPut(AutoControl_ActualStatus);
      }
      else if ( (AutoControl_ActualStatus == 0) && (AutoControl_ActualStatus != AutoControlCmd) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Updated control configuration\n");
        printTSMsg("[SEQ STEERER - TANK 5] Start Control Operations in AUTOMATIC Mode\n");
        AutoControl_ActualStatus = AutoControlCmd;
        pvPut(AutoControl_ActualStatus);
      }
      else {
        printTSMsg("[SEQ STEERER - TANK 5] This message should not appear :)\n");
      }

  } state ready_for_operation
}


/* state READY_FOR_OPERATION 	*/
  state ready_for_operation {

    entry {
      printTSMsg("\n[SEQ STEERER - TANK 5] Enter in READY FOR OPERATION status\n");
      StateMachineCode=40;
      pvPut(StateMachineCode);
    }

		/* Tank Disabled  */
    when( T_Enabling_System == 0) {
        printTSMsg("[SEQ STEERER  - TANK 5] Tank 5 Disabled by Operator\n");
        printTSMsg("[SEQ STEERER  - TANK 5] System functional control disabled. Go to TANK DISABLED\n");
    } state tank_disabled

    /* Chassis problem */
    when ( (Chassis5_enable_RB == 0) ) {
      printTSMsg("[SEQ STEERER - TANK 5] Error in chassis enable system: one or more chassis are not available\n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
    } state failure

    /* Thermal Switch problem */
    when ( (T5_V1_Termal_OkStat == 0) || (T5_H1_Termal_OkStat == 0) || (T5_V2_Termal_OkStat == 0) || (T5_H2_Termal_OkStat == 0) ) {
    /* Line disconnected (HW) */
        printTSMsg("[SEQ STEERER - TANK 5] Error in line enable system: thermal switch problem in one or more lines \n");
        printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
    } state failure

    /* 24V Error */
    when ( (Rack010_24V == 0) || (Rack030_24V == 0) ) {
        printTSMsg("[SEQ STEERER - TANK 5] Error in 24V power racks. \n");
        printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
    } state failure

    /* Problems with steerers impedances */
    when ( (T5_V1_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_H1_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_V2_AvgImpedance > MAX_AVG_DEVIANCE) || (T5_H2_AvgImpedance > MAX_AVG_DEVIANCE) ) {
  /* Line disconnected (HW) */
      printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more stereres register HIGH IMPEDANCE DEVIANCE \n");
      printTSMsg("[SEQ STEERER - TANK 5] System not ok: go to FAILURE \n");
    } state failure

		/* Error with high temperature measured related to steerers */
		when (T5_V1_ThermoSensor_Severity == 2 || T5_H1_ThermoSensor_Severity == 2 || T5_V2_ThermoSensor_Severity == 2 || T5_H2_ThermoSensor_Severity == 2) {
				printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more thermo-sensors indicate HIGH TEMPERATURE \n");
				printTSMsg("[SEQ STEERER - TANK 5] System not ok: switch off steerers with error and go to FAILURE \n");

				if (T5_V1_ThermoSensor_Severity == 2 || T5_V1_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V1. Power supply disabled.\n");
					T5_V1_ChEnableCmd = 0;
					pvPut(T5_V1_ChEnableCmd, SYNC);
					sleep(1);
				}
				if (T5_H1_ThermoSensor_Severity == 2 || T5_H1_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H1. Power supply disabled.\n");
					T5_H1_ChEnableCmd = 0;
					pvPut(T5_H1_ChEnableCmd, SYNC);
					sleep(1);
				}
				if (T5_V2_ThermoSensor_Severity == 2 || T5_V2_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V2. Power supply disabled.\n");
					T5_V2_ChEnableCmd = 0;
					pvPut(T5_V2_ChEnableCmd, SYNC);
					sleep(1);
				}
				if (T5_H2_ThermoSensor_Severity == 2 || T5_H2_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H2. Power supply disabled.\n");
					T5_H2_ChEnableCmd = 0;
					pvPut(T5_H2_ChEnableCmd, SYNC);
					sleep(1);
				}
		} state failure


		/* Error with Broken sensor temperature related to steerers */
		when (T5_V1_ThermoSensor_Module_Error == 1 || T5_H1_ThermoSensor_Module_Error == 1 || T5_V2_ThermoSensor_Module_Error == 1 || T5_H2_ThermoSensor_Module_Error == 1) {
				printTSMsg("[SEQ STEERER - TANK 5] Error in steerer system: one or more thermo-sensors indicate HIGH TEMPERATURE \n");
				printTSMsg("[SEQ STEERER - TANK 5] System not ok: switch off steerers with error and go to FAILURE \n");

				if (T5_V1_ThermoSensor_Severity == 2 || T5_V1_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V1. Power supply disabled.\n");
					T5_V1_ChEnableCmd = 0;
					pvPut(T5_V1_ChEnableCmd, SYNC);
					sleep(1);
				}
				if (T5_H1_ThermoSensor_Severity == 2 || T5_H1_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H1. Power supply disabled.\n");
					T5_H1_ChEnableCmd = 0;
					pvPut(T5_H1_ChEnableCmd, SYNC);
					sleep(1);
				}
				if (T5_V2_ThermoSensor_Severity == 2 || T5_V2_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer V2. Power supply disabled.\n");
					T5_V2_ChEnableCmd = 0;
					pvPut(T5_V2_ChEnableCmd, SYNC);
					sleep(1);
				}
				if (T5_H2_ThermoSensor_Severity == 2 || T5_H2_ThermoSensor_Module_Error == 1) {
					printTSMsg("[SEQ STEERER - TANK 5] Thermosensor error related to Steerer H2. Power supply disabled.\n");
					T5_H2_ChEnableCmd = 0;
					pvPut(T5_H2_ChEnableCmd, SYNC);
					sleep(1);
				}
		} state failure

    /* Stop Operation (for every mode of operation) */
    when ( StopOperationCmd == 1 ) {
        printTSMsg("[SEQ STEERER - TANK 5] Stop Control Operations. System goes back to Idle Status.\n");
        StartOperationCmd=0;
        pvPut(StartOperationCmd);
    } state idle

    /* Possibility to modify [MAN|AUTO] control during operations without changing state*/
    when ( AutoControl_ActualStatus != AutoControlCmd ) {
        AutoControl_ActualStatus = AutoControlCmd;
        pvPut(AutoControl_ActualStatus);
        if ( AutoControl_ActualStatus == 0 ) {
          printTSMsg("[SEQ STEERER - TANK 5] Control Configuration changed to MANUAL .\n");
        }
        else if ( AutoControl_ActualStatus == 1 ) {
          printTSMsg("[SEQ STEERER - TANK 5] Control Configuration changed to AUTOMATIC.\n");
        }
    } state ready_for_operation

    /* NOTES: */
    /* Case of Operation: RF Conditioning */
    /*
    Conditions:
     - Steerers are OFF
     - Setpoints are at 0A
    */
    /* Case of Operation: Ready for Beam */
    /*
    Conditions:
     - Steerers are ON
     - Setpoints are set at "operative" value
    */

    /* Configurations done in DTL Orchestration State Machine  */
  }



/* state FAILURE	*/
  state failure {

    entry {
        printTSMsg("\n[SEQ STEERER - TANK 5] Enter in FAILURE status\n");
        StateMachineCode=50;
        pvPut(StateMachineCode);

        /* Force total ok reset if PVs are missing */
        if (pvConnectCount() != pvAssignCount()) {
          Tank_Total_Ok_Status=0;
          pvPut(Tank_Total_Ok_Status);
        }

        /* Switch off each steerer channel */
        T5_V1_ChEnableCmd = 0;
        pvPut(T5_V1_ChEnableCmd, SYNC);
        T5_H1_ChEnableCmd = 0;
        pvPut(T5_H1_ChEnableCmd, SYNC);
        T5_V2_ChEnableCmd = 0;
        pvPut(T5_V2_ChEnableCmd, SYNC);
        T5_H2_ChEnableCmd = 0;
        pvPut(T5_H2_ChEnableCmd, SYNC);

    }

		/* Tank Disabled  */
    when( T_Enabling_System == 0) {
        printTSMsg("[SEQ STEERER  - TANK 5] Tank 5 Disabled by Operator\n");
        printTSMsg("[SEQ STEERER  - TANK 5] System functional control disabled. Go to TANK DISABLED\n");
    } state tank_disabled

    /* System should provide an healthy status to confirm failure reset */
    /* Check on: Chassis, Thermal Switches, 24V Rack Power */
    when ( (FaultReset == 1) &&  (Chassis5_enable_RB == 1) && (T5_V1_Termal_OkStat == 1) && (T5_H1_Termal_OkStat == 1) && (T5_V2_Termal_OkStat == 1) && (T5_H2_Termal_OkStat == 1) && (Rack010_24V == 1) && (Rack030_24V == 1) && (T5_V1_AvgImpedance < MAX_AVG_DEVIANCE) && (T5_H1_AvgImpedance < MAX_AVG_DEVIANCE) && (T5_V2_AvgImpedance < MAX_AVG_DEVIANCE) && (T5_H2_AvgImpedance < MAX_AVG_DEVIANCE) ) {

        printTSMsg("[SEQ STEERER - TANK 5] Failure Stage reset by Operator. \n");
        printTSMsg("[SEQ STEERER - TANK 5] Reset conditions satisfied.\n");

        /* Reset FaultReset command */
        FaultReset=0;
        pvPut(FaultReset);

    } state maintenance

  }


/* state MAINTENANCE	*/
  state maintenance {

    entry {
        printTSMsg("\n[SEQ STEERER - TANK 5] Enter in MAINTENANCE status\n");
        StateMachineCode=60;
        pvPut(StateMachineCode);
    }

		/* Tank Disabled  */
    when( T_Enabling_System == 0) {
        printTSMsg("[SEQ STEERER  - TANK 5] Tank 5 Disabled by Operator\n");
        printTSMsg("[SEQ STEERER  - TANK 5] System functional control disabled. Go to TANK DISABLED\n");
    } state tank_disabled

    /* System should provide an healthy status to confirm failure reset */
    /* Check on: Chassis, Thermal Switches, 24V Rack Power */
    when ( (MaintenanceReset == 1) &&  (Chassis5_enable_RB == 1) && (T5_V1_Termal_OkStat == 1) && (T5_H1_Termal_OkStat == 1) && (T5_V2_Termal_OkStat == 1) && (T5_H2_Termal_OkStat == 1) && (Rack010_24V == 1) && (Rack030_24V == 1) && (T5_V1_AvgImpedance < MAX_AVG_DEVIANCE) && (T5_H1_AvgImpedance < MAX_AVG_DEVIANCE) && (T5_V2_AvgImpedance < MAX_AVG_DEVIANCE) && (T5_H2_AvgImpedance < MAX_AVG_DEVIANCE) ) {

        printTSMsg("[SEQ STEERER - TANK 5] Failure Stage reset by Operator. \n");
        printTSMsg("[SEQ STEERER - TANK 5] Reset conditions satisfied.\n");

        /* Reset MaintenanceReset command */
        MaintenanceReset=0;
        pvPut(MaintenanceReset);

    } state initialization

  }


/* state DISABLED */
	state tank_disabled {

    entry {
        printTSMsg("\n[SEQ STEERER  - TANK 5] Enter in TANK DISABLED status\n");
        StateMachineCode=70;
        pvPut(StateMachineCode);
    }

    when( T_Enabling_System == 1) {
        printTSMsg("[SEQ STEERER  - TANK 5] Tank 5 Enabled by Operator\n");
        printTSMsg("[SEQ STEERER  - TANK 5] Starting TANK 5 State Machine re-initialization\n");
    } state initialization

  }


/* end statemachine */
}
