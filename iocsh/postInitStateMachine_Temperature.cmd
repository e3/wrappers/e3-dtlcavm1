#==============================================================================
# postInitStateMachine_Temperature.cmd
#- Arguments: P, TANK

#-d /**
#-d   \brief Execute DTL Temperature State Machine.
#-d   \details Configure and start SNL State Machine devoted to manage tuners system.
#-d   \author Maurizio Montis (INFN-LNL)
#-d   \modified by Alfio Rizzo (ESS)
#-d   \file
#-d   \param P System name, i.e. DTL-010
#-d   \param TANK System name, i.e. 010
#-d   \note Example call:
#-d   \code
#-d    ${SCRIPTEXEC} "$(dtlcavm1_DIR)postInitStateMachine_Temperature.cmd", "P=DTL-010, TANK=010"
#-d   \endcode
#-d */

afterInit(seq temperature_tank${TANK}  "SECTION_NAME=${P}")
